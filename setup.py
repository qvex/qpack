import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="qpack",
    version="1.3.1",
    author="QVEX",
    author_email="qvex@clubs.queensu.ca",
    description="Cross-platform, language-independent build system written in Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/qvex/qpack",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'jsonschema',
        'semver'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['qpack=qpack:main']
    }
)
