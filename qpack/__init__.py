import json
import re
import subprocess
from getopt import getopt
from os import path
from shutil import copyfile
from sys import argv

import semver as semver
from jsonschema import validate
from pkg_resources import get_distribution

from qpack.profile import profiles
from qpack.util import select, input_list, input_pred, lookup_path, unescape


def version():
    return get_distribution('qpack').version


def validate_config(config):
    '''Validate a config structure against the JSON schema.

    :param config: The config structure to validate.
    '''

    with open(path.join(path.dirname(__file__), 'project_schema.json'), 'r') as file:
        schema = json.load(file)
    validate(config, schema)


def get_proj_file(args):
    '''Determines the path of the project file based on the options provided.

    :param args: The command-line arguments provided.
    '''

    return args[0] if len(args) == 1 else 'qpack.json'


def read_config(args):
    '''Locate and load a JSON configuration file based on the given command-line arguments.

    :param args: The command-line arguments after getopt processing.
    :return: A 2-tuple containing the Python representation of the JSON configuration file and the project directory.
    '''

    # Load from file indicated by first argument, or 'qpack.json' if none specified
    proj_file = get_proj_file(args)
    with open(proj_file, 'r') as file:
        config = json.load(file)

    # Migrate format if it is from an older, incompatible version of QPack
    if not ('qpackVersion' in config and semver.match(config['qpackVersion'], '>=1.0.0')):
        if 'dependencies' in config:
            for d in config['dependencies']:
                if type(d) is dict:
                    if 'git' in d and type(d['git']) is list:
                        d['git'] = ' '.join(d['git'])
                    if 'build' in d and type(d['build']) is list:
                        d['build'] = ' '.join(d['build'])
                    if 'clean' in d and type(d['clean']) is list:
                        d['clean'] = ' '.join(d['clean'])
        if 'compilerArgs' in config and type(config['compilerArgs']) is dict:
            for lang, args in config['compilerArgs'].items():
                if type(args) is list:
                    config['compilerArgs'][lang] = ' '.join(args)
        if 'linkerArgs' in config and type(config['linkerArgs']) is list:
            config['linkerArgs'] = ' '.join(config['linkerArgs'])
        config['qpackVersion'] = version()

    # Validate using schema
    validate_config(config)

    return config, path.dirname(proj_file)


def search_opt(optlist, name):
    '''Enumerate the values from optlist where the key matches name.

    :param optlist: The list of options from getopt.
    :param name: The option name to look for.
    :return: A generator for the values corresponding with name.
    '''

    return (value for key, value in optlist if key == name)


def get_indent(optlist):
    '''Determines the JSON indentation style to use based on the options provided.

    :param optlist: The parsed command-line options.
    '''
    indent = next(search_opt(optlist, '--indent'), None)
    if indent == 'tab':
        indent = '\t'
    elif indent == 'none':
        indent = None
    elif indent is None:
        indent = 2
    else:
        indent = int(indent)
    return indent


def apply_set(config, query, print_selector=True):
    '''Applies a --set query.

    :param config: The config structure to apply the query to.
    :param query: The query to apply.
    :param print_selector: True if the selector string should be printed; false otherwise.
    '''
    pos_eq = next(re.finditer('[^\\\\]:=', query)).start() + 1
    key = query[:pos_eq]
    value = query[pos_eq + 2:]
    if print_selector:
        print(key)
    if value[0] == '%':
        value = {
            'true': True,
            'false': False,
            'object': {},
            'array': []
        }[value[1:]]
    else:
        value = unescape(value)
    for getter, setter in lookup_path(config, key):
        setter(value)


def apply_overrides(config, proj_dir):
    '''Looks for a .qpack_overrides file in the current directory and applies it.

    :param config: The config structure to apply the overrides to.
    :param proj_dir: The project directory.
    '''
    file_path = path.join(proj_dir, '.qpack_overrides')

    if not path.isfile(file_path):
        return

    with open(file_path) as file:
        for l in file:
            apply_set(config, l.strip(), False)


def main():
    '''Main function for QPack.

    :return: Exit code.
    '''

    # Parse command-line arguments and options
    optlist, args = getopt(argv[1:], 'hbiceadg',
                           ['help', 'version', 'build', 'init', 'clean', 'edit', 'all', 'deps-only',
                            'name=', 'profile=', 'indent=', 'source=', 'get=', 'set=', 'del=', 'no-source', 'git'])
    if len(args) > 1:
        print('Only 1 project file may be specified!')
        print(repr(args))
        exit(1)

    # Extract just the names of options
    optnames = [opt for opt, value in optlist]

    # Run the requested subcommand
    if '-h' in optnames or '--help' in optnames:
        print('QPack is a cross-platform, language-independent build system written in Python. See '
              'https://qvex.ca/qpack for details and documentation.')

    elif '--version' in optnames:
        print(version())

    elif '-b' in optnames or '--build' in optnames:
        # Load the config file and profile
        config, proj_dir = read_config(args)
        apply_overrides(config, proj_dir)
        if config['profile'] not in profiles:
            print(f'Unknown profile "{config["profile"]}"')
            exit(1)
        profile = profiles[config['profile']]

        # Clean if requested
        clean = False
        deps_only = '-d' in optnames or '--deps-only' in optnames
        if '-a' in optnames or '--all' in optnames:
            clean = True
            if not deps_only:
                profile.clean(config, proj_dir)

        # Process dependencies
        if 'dependencies' in config:
            for d in config['dependencies']:
                if type(d) is str:
                    d = {
                        'path': d,
                        'type': 'qpack'
                    }
                if 'git' in d:
                    subprocess.run('git pull ' + d['git'], cwd=d['path'], shell=True)
                if d['type'] == 'qpack':
                    subprocess.run('qpack ' + ('-ba ' if clean else '-b ') + path.join(d['path'], 'qpack.json'), shell=True)
                elif d['type'] == 'make':
                    if clean:
                        subprocess.run('make clean', cwd=d['path'], shell=True)
                    subprocess.run('make', cwd=d['path'], shell=True)
                elif d['type'] == 'cmake':
                    if clean:
                        subprocess.run(d['clean'], cwd=d['path'], shell=True)
                    subprocess.run('cmake --build', cwd=d['path'], shell=True)
                elif d['type'] == 'custom':
                    if clean:
                        subprocess.run(d['clean'], cwd=d['path'], shell=True)
                    subprocess.run(d['build'], cwd=d['path'], shell=True)
        else:
            config['dependencies'] = []

        # Build the project
        if not deps_only:
            profile.build(config, proj_dir)

    elif '-i' in optnames or '--init' in optnames:
        # Determine location of new config file
        proj_file = get_proj_file(args)

        # Determine project name
        name = (next(search_opt(optlist, '--name'), None) or
                input_pred('Enter the project name (non-empty, no slashes): ',
                           lambda x: x.strip() and '/' not in x)).strip()

        # Determine profile
        profile_name = next(search_opt(optlist, '--profile'), None) \
            or select('Enter the profile (%s): ', profiles.keys())
        profile = profiles[profile_name]

        # Determine source directories
        source = list(search_opt(optlist, '--source')) or ([] if '--no-source' in optnames else input_list(
            'Enter the source directories (relative to the project file location):'))

        # Create config object
        config = {
            'qpackVersion': version(),
            'name': name,
            'profile': profile_name,
            'source': source[0] if len(source) == 1 else source,
            **(profile.init(source) or {})
        }

        # Generate config file
        with open(proj_file, 'w+') as file:
            json.dump(config, file, indent=get_indent(optlist))
        
        # If requested, generate gitignore file
        if '-g' in optnames or '--git' in optnames:
            copyfile(path.join(path.dirname(__file__), "default.gitignore"),
                     path.join(path.dirname(proj_file), '.gitignore'))

    elif '-c' in optnames or '--clean' in optnames:
        # Load config and clean project
        config, proj_dir = read_config(args)
        apply_overrides(config, proj_dir)
        profiles[config['profile']].clean(config, proj_dir)

    elif '-e' in optnames or '--edit' in optnames:
        # Load config and query/edit as requested
        config, proj_dir = read_config(args)
        for opt, value in optlist:
            if opt == '--get':
                print(value)
                for getter, setter in lookup_path(config, value):
                    print('\t' + str(getter()))
            elif opt == '--set':
                apply_set(config, value)
            elif opt == '--del':
                print(value)
                for getter, setter in lookup_path(config, value):
                    setter(None)

        # Validate and save updated config
        validate_config(config)
        with open(get_proj_file(args), 'w+') as file:
            json.dump(config, file, indent=get_indent(optlist))

    else:
        print('Must supply one of: -b, --build, -i, --init, -c, --clean, -e, --edit')
        exit(1)
