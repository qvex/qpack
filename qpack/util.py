import re
from itertools import islice
from os import path, listdir


def get_files(file_path):
    """Enumerate the files in a path recursively.

    :param file_path: The path to start from.
    :return: A generator for the paths to the files in file_path.
    """

    if path.isfile(file_path):
        # If the path refers to a file, yield it
        yield file_path

    elif path.isdir(file_path):
        # If the path refers to a directory, recurse
        for name in listdir(file_path):
            yield from get_files(path.join(file_path, name))


def get_sources(config, proj_dir):
    """Parse the 'source' entry in a project config and enumerate all files in the source directory tree.

    :param config: The project config object.
    :param proj_dir: The project directory.
    :return: A generator for the paths to all files in the source directory tree.
    """

    source = config['source']

    # Differentiate between JSON string and list form for the 'source' entry, then delegate to get_files
    return get_files(path.join(proj_dir, source)) if type(source) == str else (file for entry in source for file in
                                                                               get_files(path.join(proj_dir, entry)))


def select(string, options):
    """Ask the user to select an item from a list.

    :param string: The prompt, with a format specifier to insert the comma-separated options.
    :param options: The options to present.
    :return: The selected option.
    """

    # Format prompt
    string %= ', '.join(options)

    # Ask user until their input is a valid option
    while True:
        result = input(string)
        if result in options:
            return result


def input_list(prompt):
    """Ask the user to input a list of strings.

    :param prompt: The prompt.
    :return: The list of strings.
    """

    # Print the prompt, if present
    if prompt:
        print(prompt)

    # Read lines until the user enters a blank line
    result = []
    while True:
        line = input('Next or blank to exit: ')
        if line:
            result.append(line)
        else:
            return result


def input_pred(prompt, predicate):
    """Ask the user for input that passes a predicate.

    :param prompt: The prompt.
    :param predicate: The predicate.
    :return: The validated input.
    """

    # Read until the input is valid
    while True:
        result = input(prompt)
        if predicate(result):
            return result


def raise_(error):
    raise error


def set_value(data, index, value):
    if value is None:
        del data[index]
    else:
        data[index] = value


def unescape(value):
    return re.sub('\\\\(.)', '\1', value)


def lookup_path(data, path, fail_silently=False, raw_setter=None):
    """Generates a getter and setter function for a value or set of values in a data structure identified by a path.
    :param data: The data structure to navigate through.
    :param path: The path to look up.
    :param fail_silently: Whether to return none instead of raising an error.
    """

    # Determine the type of lookup
    if path == '':
        # No lookup to be performed, just refer to the provided data
        return [(lambda: data,
                 raw_setter or (lambda value: raise_(ValueError('Cannot write to pass-by-value parameter'))))]

    elif path[0] == '[':
        # Query an array
        if type(data) is not list:
            if fail_silently:
                return []
            raise ValueError('Cannot iterate over non-array value')

        if path[:3] == '[*]':
            # Return all array items
            return (pair for item in data for pair in
                    lookup_path(item, path[3:], fail_silently, lambda value: set_value(data, data.index(item), value)))

        if path[:2] == '[@':
            # Addressing specific element
            if path[2] == ']':
                # Append item
                return lookup_path(None, path[3:], fail_silently, lambda value: data.append(value))
            else:
                # Addressed by index
                pos_end = path.index(']')
                index = int(path[2:pos_end])
                return lookup_path(data[index], path[pos_end + 1:], fail_silently,
                                   lambda value: set_value(data, index, value))

        # Extract values to process; allow simple character escapes
        pos_eq = path.index('=')
        pos_end = next(re.finditer('[^\\\\]\\]', path[pos_eq:])).start() + pos_eq + 1

        # Process the items
        items = ((item, list(lookup_path(item, path[1:pos_eq], True))) for item in data)
        filtered = (item for item, query in items
                    if len(query) and query[0][0]() == unescape(path[pos_eq + 1:pos_end]))
        return (pair for item in filtered
                for pair in lookup_path(item, path[pos_end + 1:], fail_silently,
                                        lambda value: set_value(data, data.index(item), value)))

    elif path[0] == '.':
        # Get a property
        if type(data) is not dict:
            if fail_silently:
                return []
            raise ValueError('Cannot get a property of a non-object value')

        # Extract property name
        delim = list(islice(re.finditer('[\\[.]', path[1:]), 1))
        name = path[1:delim[0].start() + 1] if len(delim) else path[1:]

        # Extract property from object
        return lookup_path(data[name] if name in data else None, path[len(name) + 1:], fail_silently,
                           lambda value: set_value(data, name, value))

    else:
        raise ValueError('Invalid path format')
