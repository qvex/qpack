# QPack

Cross-platform, language-independent build system
written in Python

## Concept

QPack exists solely because I couldn't find a good
way to set up multiple inter-dependent projects
using standard tools such as CMake. QPack is
primarily designed to run on POSIX systems (i.e.
Linux, OS X, BSD, etc.), but can run on Windows so
long as there is a substitute `make` and compatible
compiler toolchain installed.

QPack uses a JSON file to describe a "project". This
file indicates what type of compileation to perform
(called the "profile"; currently the only supported
profile is `c_cxx_native`, i.e. compiling C/C++ code,
as this is the use case that QPack was created for).
The further available options are dependent on the
profile selected for the project.

## Command-line Interface

QPack is invoked via the `qpack` command. There are
broadly three main subcommands:

-   `-b` or `--build`: Build the project and its
    dependencies. Additionally, `-a` or `--all` can
    be specified to force a clean build.
-   `-c` or `--clean`: Clean the project. For C/C++
    projects,this simply removes all files in the
    `qpack-build` directory.
-   `-i` or `--init`: Initialize a new project (i.e.
    create the project JSON file). This command is
    normally interactive, however any or all inputs
    can be provided via command-line options,
    allowing it to be used in scripts; see below for
    more details.
-   `-e` or `--edit`: Query the project configuration
    parameters. See below for syntax.

For all subcommands, the project file can be
specified as a command-line argument; if omitted, it
is assumed to be `qpack.json` in the current
directory. 

### Creating a Project

When creating a new project, the following pieces of
information are required:

-   Project name. This is used to generate the name
    of the output file. It can be specified on the
    command line with the `--name` option.
-   Profile. This can be specified on the command
    line with the `--profile` option
-   Source directories. These are the locations
    (paths relative to the location of the project
    file) where QPack will look for source code.
    They can be specified with the `--source`
    option; multiple are allowed. To force an empty
    set of source directories without an
    interactive prompt, use the `--no--source`
    option.

The option `--indent` may also be specified to
customize the indentation of the generated JSON
project file. If set to a number, the file is
indented with that number of spaces. If set to
`tab`, the file is indented with tab characters.
If set to `none`, no excess whitespace will be
added to the file (including newlines). The default
value is `2`. Additionally, if the -g or --git flag
is specified, it will automatically add an appropriate
.gitignore file in the project directory.

#### Examples

Create a new project interactively:

    qpack -i

Create a new project interactively, with a custom
project file name:

    qpack -i project.json

Create a new project non-interactively:

    qpack -i --name example --profile c_cxx_native --source src

### Querying the Project Configuration

By supplying the `-e` or `--edit` option, one or more
configuration parameters may be retrieved or
modified. Configuration parameters are identified by
a string consisting of a series of selectors. The
following types of selectors are supported:

-   `[*]`: Select all elements of an array.
-   `[@7]`: Select element at index `7` of an array.
-   `[.prop=value]`: Select all elements of an array
    which are objects with a property named `prop`
    that is set to `value`.
-   `.prop`: Select the value of the property named
    `prop` on an object.

Each query consists of either a `--get` or a `--set`
option. For `--get` queries, the option value is
expected to be a selector string. A `--get` query
that produces multiple results will print out all
results. For `--set` queries, the option value is
expected to be a selector string, followed by `:=`,
followed by a value. This will set the value of all
properties selected by the query. A preceding
backslash may be used to escape any character in the
option value.

The following special values are supported for
`--set` queries:

Value|JSON Equivalent
-----|---------------
`%true`|`true`
`%false`|`false`
`%object`|`{}`
`%array`|`[]`

#### Examples

Get the project profile type:

    qpack -e --get .profile

Get the path to every dependency that is a CMake
project:

    qpack -e --get .dependencies[.type=cmake].path

Set the C++ compiler to clang++:

    qpack -e --set .compilers.cxx:=clang++

Set all dependencies to copy to output:

    qpack -e --set .dependencies[*].copyOutput:=%true

## Project File Format

The following fields are supported in project files
regardless of profile:

Field name    |Description             |Required|Type
--------------|------------------------|--------|----
`name`        |The project name        |yes     |string
`profile`     |The profile             |yes     |string
`source`      |The source directories  |yes     |string (if only one) or array of strings
`dependencies`|The project dependencies|no      |array of dependency specifiers (see below)

Dependency specifiers are normally objects with the
following fields:

Field name  |Description                                                         |Required|Type
------------|--------------------------------------------------------------------|--------|----
`path`      |The location of the dependency directory                            |yes     |string
`type`      |The type of dependency; currently only `qpack` is fully supported   |yes     |string
`git`       |Set to automatically `git pull` the dependency during compilation   |no      |string

Note that the value of `git` is appended
to `git pull` as command line arguments (example
value might be `"origin master"`). For C/C++
projects, the field `copyOutput` (a boolean) can
also be used to specify that the output files of
this dependency should be copied to the output of
the main project; it is effectively `false` by
default.

Additionally, for C/C++ projects, the following
fields can be specified:

Field name    |Description                                                                    |Required|Type
--------------|-------------------------------------------------------------------------------|--------|----
`mode`        |The type of output - can be one of `executable`, `static`, `shared` or `object`|yes     |string
`compilers`   |The compilers to use for various languages                                     |no      |object mapping string to string
`libraries`   |The libraries to link the output with                                          |no      |array of strings
`compilerArgs`|Any additional arguments to pass to the compiler                               |no      |object mapping string to string
`linkerArgs`  |Any additional arguments to pass to the linking program                        |no      |object mapping string to string

On new projects, `mode` is initialized to
`executable`, and no other profile-specific fields
are added.

The `compilers` field allows you to override the
system compiler with a custom one - for example, to
force compliation to use clang over gcc, or to
cross-compile for an embedded target. The value maps
languages (namely `c` and `cxx`) to programs.

A similar format is used for `compilerArgs`,
specifying a list of arguments for each language.
`linkerArgs` is expected to just be an string of
arguments, as the linker is only invoked once
whereas compilation occurs for each translation
unit (which may be in C or C++). Note that the
options specified hear should be ready to be passed
to the high-level command used to link the output,
which is normally a compiler rather than `ld` (or
equivalent) itself.

Use `libraries` as a shorthand for putting
appropriate `-l` options in `linkerArgs`. Note that
the output directory of each dependency is
automatically added to the library path. Similarly,
the source directories of each dependency are
automatically added to the user include path.

## Overrides

Platform-specific configuration overrides can be
applied via a `.qpack_overrides` file in the same
directory as the project configuration file. Each
line in this file is applied to the configuration in
the same way as the value of a `--set` option value,
however the updated configuration is kept in memory
only (i.e. the modifications made by the overrides
are not written to disk). These files are intended to
be user-specific, and should be ignored by version
control systems. 
